## #2 Choix du DP : MVC
Le MVC permet d'organiser ses fichiers sources de sorte à séparer la vue d'une page web (HTML, CSS), des données modèles (modèle BDD) et enfin les controleurs qui vont faire le lien entre tous les fichiers modèle et qui vont appréhender les requetes recues/envoyées ainsi que gérer les routes.
Ici les vues ne nous servirons pas à grand chose car nous avons rien a afficher.

## #3 SOLID : Ségregation des interfaces
Permet de découper une interface volumineuse en plusieurs interfaces.
Cette factorisation permet de maintenir plus facilement ces interfaces et de modifier rapidement sans devoir changer plusieurs fichiers.

## #4 Adaptateur
L'adaptateur nous permet de modifier la nature d'un objet pour l'adapter à celle d'un autre objet.

## #5 Facade
La facade nous permet d'abstraire les méthodes pour simplifier l'utilisation et pour cacher les rouages derrière une interface.

## #6 Diagramme de classe
<img src="./diagram_final.jpg"/>