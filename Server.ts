#!/usr/bin/env node
import express from 'express'
import App from './App'
import http from 'http'

export default class Server {
  public port : number | boolean;
  public app: any;

  constructor(_app: express.Application) {
    this.port = this.normalizePort(process.env.PORT || 1234)
    this.app = _app;
  }

  public start(): void{
    this.app.set('port', this.port)
    const server = http.createServer(this.app)
    server.listen(this.port)
    server.on('error', (error) => this.onError(error))
    server.on('listening', () => this.onListening())
}

  protected normalizePort(value: any): number | boolean{
    const port = parseInt(value, 10)

    if (isNaN(port)) {
      return value
    }
  
    if (port >= 0) {
      return port
    }
  
    return false
  }

  protected onListening(): void{
    console.log(`Listening on ${this.port}`);
  }

  protected onError(error: any): never{
    if (error.syscall !== 'listen') {
        throw error
      }
    
      switch (error.code) {
        case 'EACCES':
          console.error(`${this.port} requires elevated privileges`)
          process.exit(1)
        case 'EADDRINUSE':
          console.error(`${this.port} is already in use`)
          process.exit(1)
        default:
          throw error
      }  }
}