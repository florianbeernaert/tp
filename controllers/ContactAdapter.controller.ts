import { Contact } from '../type'

export default class ContactAdapter implements Contact{
    fullname: string;
    email: string;
    phone: string;
    address: string;
    postalCode: string;
    city: string;

    constructor() {
        this.fullname = "***";
        this.email = "***";
        this.phone = "***";
        this.address = "***";
        this.postalCode = "***";
        this.city = "***";
    }
}
