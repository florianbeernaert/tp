import Storage from '../models/Storage'
import {Request, Response } from 'express'
import ContactAdapter from './ContactAdapter.controller';
import Order from 'type';

export default class Controller {
    private storage: Storage;

    constructor() {
        this.storage = new Storage('orders');
    }

    public getHelloWorld(req: Request, res: Response){
        res.send('Hello World !');
    }

    public getFavicon(req: Request, res: Response){
        res.status(204);
    }

    public async getOrders(req: Request, res: Response){
        const orders:Order[] = await this.storage.getItem();
        orders.map(order => {
            order.delivery.contact = new ContactAdapter();
        })
        res.json(orders);
    }

    public async getOrdersById(req: Request, res: Response){
        const id = req.params.id;
        const orders = await this.storage.getItem();
        const order = orders.find((order: any) => order.id ===parseInt(id,10));
        if (!order) {
           res.sendStatus(404);
        } else {
            res.json(order);
        } 
    }

    public async postOrders(req: Request, res: Response){
        const orders = await this.storage.getItem();
        const alreadyExists =  orders.find((order: any) => order.id === req.body.id);
        if (alreadyExists) {
           res.sendStatus(409);
        } else {
            orders.push(req.body);
            await this.storage.setItem(orders);
            res.sendStatus(201);
        }
    }
}