import storage from 'node-persist'
import { orders } from '../data/_orders'

export default class Storage {
    private type: string;

    constructor(type: string) {
        this.type = type;
        storage.init().then(() => storage.setItem(this.type, orders));
    }

    public async getItem(): Promise<any> {
        return await storage.getItem(this.type);
    }

    public async setItem(item: string): Promise<any> {
        return await storage.setItem(this.type, item);
    }
}