import { Router, Request, Response } from 'express'
import Controller from '../controllers/Controller'


const router = Router();
const controller = new Controller();

router.get('/', async (req: Request, res: Response) => {
  controller.getHelloWorld(req, res);
})

router.get('/favicon.ico', (req: Request, res: Response) => {
  controller.getFavicon(req, res);
})

export default router;