import { Router, Request, Response } from 'express'
import Controller from '../controllers/Controller'

const router = Router();
const controller = new Controller();

router.get('/', async (req: Request, res: Response) => {
  controller.getOrders(req, res);
})

router.get('/:id', async (req: Request, res: Response) => {
  controller.getOrdersById(req, res);
})

router.post('/', async (req: Request, res: Response) => {
  controller.postOrders(req, res);
})

export default router;