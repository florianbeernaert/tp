export default interface Order {
    id: number;
    packages: Package[];
    delivery: Delivery
}

export interface Package {
    length: Measure;
    width: Measure;
    height: Measure;
    weight: Measure;
    products: Product[];
}

export interface Measure {
    unit: string;
    value: number;
}

export interface Product {
    name: string;
    price: Monetary;
}

export interface Monetary {
    currency: string;
    value: number;
}

export interface Delivery {
    storePickingInterval: Interval;
    deliveryInterval: Interval;
    contact: Contact;
    carrier: Carrier;
}

export interface Interval {
    start: string;
    end: string;
}

export interface Contact {
    fullname: string;
    email: string;
    phone: string;
    address: string;
    postalCode: string;
    city: string;
}

export interface Carrier {
    name: string;
}
