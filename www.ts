import App from './App';
import Server from './Server';

const app = new App();
const server = new Server(app.instance);
server.start();